import java.util.Scanner;

public class Mittelwert_02 {

	public static void main(String[] args) {

		// Eingabe
		Scanner myScanner = new Scanner(System.in);

		programmhinweis("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		// System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");

		// Deklaration von Variablen
		/*double zahl1 = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
		double zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl ein: ");*/
		double summeDerEingaben = 0;
		double m;
		int anzahlWerte;

		anzahlWerte = eingabeAnzahl(myScanner, "Geben Sie die Anzahl der einzugebenden Zahlen: ");

		for(int i = 0; i < anzahlWerte; i++) {
			summeDerEingaben += eingabeDouble(myScanner, "Neuer Wert eingeben: ");
		}
		
		m = berechneMittelwert(summeDerEingaben, anzahlWerte);

		// System.out.println("Dieses Programm berechnet den Mittelwert zweier
		// Zahlen.");
		// System.out.print("Bitte geben Sie die erste Zahl ein: ");
		// zahl1 = myScanner.nextDouble();

		// System.out.print("Bitte geben Sie die zweite Zahl ein: ");
		// zahl2 = myScanner.nextDouble();

		// Verarbeitung
		// m = (zahl1 + zahl2) / 2.0;
		// m = berechneMittelwert(zahl1, zahl2);

		// Ausgabe
		// System.out.println("Mittelwert: " + m);
		ausgabe(m);

		myScanner.close();
	}
	
	public static void programmhinweis(String text) {
		System.out.println(text);
	}

	public static int eingabeAnzahl(Scanner ms, String text ) {
		System.out.print(text);
		int zahl = ms.nextInt();
		return zahl;
	}
	
	public static double eingabeDouble(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}

	public static double eingabe(Scanner myScanner, String text) {
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}

	/*public static double berechneMittelwert(double zahl1, double zahl2) {
		double m;
		m = (zahl1 + zahl2) / 2.0;
		return m;
	}*/
	
	public static double berechneMittelwert(double zahl, int anzahl) {
		double m = zahl/ anzahl;
		return m;
	}

	/* public static void ausgabe(double m) {
		System.out.println("Mittelwert: " + m);
	} */
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + mittelwert);
	}
}