import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		// Benutzereingaben lesen
		double nettogesamtpreis;
		double bruttogesamtpreis;
		String rechnungausgeben;
		String artikel = liesString(scanner, "Was m�chten Sie bestellen?");
		int anzahl = liesInt(scanner, "Geben Sie die Anzahl ein: ");
		double preis = liesDouble(scanner, "Geben Sie den Nettopreis ein: ");
		double mwst = liesDouble(scanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein: ");

		// Verarbeiten
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

		scanner.close();
	}

	public static String liesString(Scanner scanner, String text) {
		System.out.println(text);
		String artikel = scanner.next();
		return artikel;
	}

	public static int liesInt(Scanner scanner, String text) {
		System.out.println(text);
		int anzahl = scanner.nextInt();
		return anzahl;
	}

	public static double liesDouble(Scanner scanner, String text) {
		System.out.println(text);
		double preis = scanner.nextDouble();
		return preis;
	}

	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}

	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}