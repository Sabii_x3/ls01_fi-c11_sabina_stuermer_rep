
public class Konsolenausgabe_02 {

	public static void main(String[] args) {
		
		// A1.6: AB "Ausgabeformatierung 2"
		
		// AUFGABE 01
		String s = "**";
		System.out.printf(" %6.2s\n", s);
		System.out.printf("%4.1s ", s);
		System.out.printf("%4.1s\n", s);
		System.out.printf("%4.1s ", s);
		System.out.printf("%4.1s\n", s);
		System.out.printf("%7.2s\n", s);
		
		// AUFGABE 02
		// 28
		/*String a = "0!";
		String b = "1!";
		String c = "2!";
		String d = "3!";
		String e = "4!";
		String f = "5!";
		System.out.printf("%-5a\n", a);
		System.out.printf("%-5b\n", b);
		System.out.printf("%-5c\n", c);
		System.out.printf("%-5d\n", d);
		System.out.printf("%-5e\n", e);
		System.out.printf("%-5f\n", f);*/
		
		// AUFGABE 03
		
		String f = "Fahrenheit";
		String c = "Celsius";
		String z = "--------------------------";
		
		int a = 20;
		int b = 10;
		int d = 0;
		int e = 20;
		int g = 30;
		
		double h = 28.89;
		double i = 23.33;
		double j = 17.78;
		double k = 6.67;
		double l = 1.11;
		
		
		System.out.printf("%-12s | ", f);
		System.out.printf("%10s\n", c);
		System.out.printf("%-22s\n", z);
		System.out.printf("%-12d | %10.2f\n", -a, -h);
		System.out.printf("%-12d | %10.2f\n", -b, -i);
		System.out.printf("+%-11d | %10.2f\n", d, -j);
		System.out.printf("+%-11d | %10.2f\n", e, -k);
		System.out.printf("+%-11d | %10.2f\n", g, -l);
		
		

	}

}
