
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		// Aufgabe 03
		
		String f = "Fahrenheit";
		String c = "Celsius";
		String z = "--------------------------";
		
		int a = 20;
		int b = 10;
		int d = 0;
		int e = 20;
		int g = 30;
		
		double h = 28.89;
		double i = 23.33;
		double j = 17.78;
		double k = 6.67;
		double l = 1.11;
		
		
		System.out.printf("%-12s | ", f);
		System.out.printf("%10s\n", c);
		System.out.printf("%-22s\n", z);
		System.out.printf("%-12d | %10.2f\n", -a, -h);
		System.out.printf("%-12d | %10.2f\n", -b, -i);
		System.out.printf("+%-11d | %10.2f\n", d, -j);
		System.out.printf("+%-11d | %10.2f\n", e, -k);
		System.out.printf("+%-11d | %10.2f\n", g, -l);

	}

}
