
public class Konsolenausgabe {

	public static void main(String[] args) {
		System.out.print("Hello\n"); // Ausgabe: Hello
		System.out.println("Guten Morgen!"); // Ausgabe: Guten Morgen!
		
		// A1.5: AB "Ausgabeformatierung 1"
		
		//AUFGABE 01
		
		System.out.print("Er sagte: \"Guten Tag!\". ");
		System.out.printf("Mein Name ist %s. Wie %s bist du?%n", "Sabina", "alt");
		int alter = 25;
		String name = "Sabina";
		System.out.println("Ich hei�e " + name + " und bin " + alter + " Jahre alt.\n"); // Ausgabe: Ich hei�e Sabina und bin 25 Jahre alt.
		
		// Die einfache print Anweisung gibt einen Text heraus, mit println kommt nach dem Text ein zus�tzlicher Zeilenumbruch.
		// Diesen Zeileumbruch m�sste man bei der print Anweisung mit einem "\n" bewerkstelligen am Ende eines Textes
		
		
		//AUFGABE 02
		
		/*System.out.println("\t        *");
		System.out.println("\t       ***");
		System.out.println("\t      *****");
		System.out.println("\t     *******");
		System.out.println("\t    *********");
		System.out.println("\t   ***********");
		System.out.println("\t   ************");
		System.out.println("\t       ***");
		System.out.println("\t       ***");*/
		
		String s = "************";
		System.out.printf("%7.1s\n", s);
		System.out.printf("%8.3s\n", s);
		System.out.printf("%9.5s\n", s);
		System.out.printf("%10.7s\n", s);
		System.out.printf("%11.9s\n", s);
		System.out.printf("%12.11s\n", s);
		System.out.printf("%13s\n", s);
		System.out.printf("%8.3s\n", s);
		System.out.printf("%8.3s\n", s);
		
		
		// AUFGABE 03
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f\n" , a);
		System.out.printf("%.2f\n" , b);
		System.out.printf("%.2f\n" , c);
		System.out.printf("%.2f\n" , d);
		System.out.printf("%.2f\n" , e);
		
	}

}
