﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		char neustart;
		System.out.print("Fahrkartenbestellvorgang: \n");
		System.out.print("========================= \n");
		do {
			
		
		double gesamtBetrag = fahrkartenbestellungErfassen(tastatur);
		double rückgabebetrag = fahrkartenBezahlen(tastatur, gesamtBetrag);
		//fahrkartenAusgeben();
		warte(250);
		rueckgeldAusgeben(rückgabebetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n\n"
				+ "Mit der Eingabe von J für Ja können Sie noch weitere Fahrkarten bestellen.\n");
		neustart = tastatur.next().charAt(0);
		}
		while(neustart == 'J' || neustart == 'j');
		System.out.println("Wir wünschen Ihnen eine gute Fahrt!\n");
			
		tastatur.close();
	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
		double wunschkarte = tastatur.nextDouble();
		
		if(wunschkarte == 1) {
			System.out.print("Ihre Wahl: 1\n");
			wunschkarte = 2.9;
		}
		else if(wunschkarte == 2) {
			System.out.print("Ihre Wahl: 2\n");
			wunschkarte = 8.6;
		}
		else if(wunschkarte == 3) {
			System.out.print("Ihre Wahl: 3\n");
			wunschkarte = 23.5;
		}
		else {
			System.out.println("Fehler. Geben Sie bitte eine der aufgeführten Fahrkarten-Nummern an. Das Programm wurde beendet.\n");
			System.exit(0);
		}
		
		int anzahlTickets = 0;
		double gesamtBetrag = 0;
		do {
			System.out.println("\nWählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			anzahlTickets = tastatur.nextInt();
			System.out.print("Anzahl der Tickets: " + anzahlTickets);
		}
		while(anzahlTickets < 1 || anzahlTickets > 10);
		
		gesamtBetrag = wunschkarte * anzahlTickets;
		return gesamtBetrag;
	}
		
	public static double fahrkartenBezahlen(Scanner tastatur, double gesamtBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < (gesamtBetrag)) {
			System.out.printf("\nNoch zu zahlen: %.2f EURO\n",
					(gesamtBetrag) - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - (gesamtBetrag);
		return rückgabebetrag;
	}
	/*public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n");
	} */
	
	public static void warte(int millisekunden) {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunden);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n");
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				//System.out.println("2 EURO");
				muenzeAusgeben(2, "EURO");
				double temp = Math.round((rückgabebetrag -= 2) * 100);
				rückgabebetrag = (double)temp/100;
				// rückgabebetrag -= 2.0f;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				//System.out.println("1 EURO");
				muenzeAusgeben(1, "EURO");
				double temp = Math.round((rückgabebetrag -= 1) * 100);
				rückgabebetrag = (double)temp/100;
				// rückgabebetrag -= 1.0f;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				//System.out.println("50 CENT");
				muenzeAusgeben(50, "CENT");
				double temp = Math.round((rückgabebetrag -= 0.5) * 100);
				rückgabebetrag = (double)temp/100;
				// rückgabebetrag -= 0.5f;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				//System.out.println("20 CENT");
				muenzeAusgeben(20, "CENT");
				double temp = Math.round((rückgabebetrag -= 0.2) * 100);
				rückgabebetrag = (double)temp/100;
				// rückgabebetrag -= 0.2f;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				//System.out.println("10 CENT");
				muenzeAusgeben(10, "CENT");
				double temp = Math.round((rückgabebetrag -= 0.1) * 100);
				rückgabebetrag = (double)temp/100;
				// rückgabebetrag -= 0.1f;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				//System.out.println("5 CENT");
				muenzeAusgeben(5, "CENT");
				double temp = Math.round((rückgabebetrag -= 0.05) * 100);
				rückgabebetrag = (double)temp/100;
				// rückgabebetrag -= 0.05f;
			}
		}
	}
}