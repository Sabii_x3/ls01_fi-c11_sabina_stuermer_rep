import java.util.Scanner;

public class Steuersatz {

	public static void main(String[] args) {
		
		int netto = 0;
		char steuersatz;
		
		Scanner myScanner = new Scanner(System.in);
		
		netto = inputZahl(myScanner, "Geben Sie bitte den Nettowert ein: ");
		// Folgende Zeile: String geht nicht, nur char
		steuersatz = inputChar(myScanner, "Wenn der volle Steuersatz angewendet werden soll, best�tigen Sie bitte mit \"J\", andernfalls mit \"N\"");
		
		if(steuersatz == 'J' || steuersatz == 'j') {
			System.out.println("Es soll der volle Steuersatz angewendet werden.");
		}
		else if(steuersatz == 'N' || steuersatz == 'n') {
			System.out.println("Es soll der erm��igte Steuersatz angewendet werden.");
		}
	}
	
	public static int inputZahl(Scanner scanner, String text) {
		System.out.println(text);
        int z = scanner.nextInt();
        return z;
	}
	
	public static char inputChar(Scanner scanner, String text) {
		System.out.println(text);
		char c = scanner.next().charAt(0);
		return c;
	}

}
