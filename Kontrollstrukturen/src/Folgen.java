// Aufgabe 4: Folgen (2 Stars)
public class Folgen {

	public static void main(String[] args) {
		int zahl = 0;
		System.out.println("Aufgabe 4 - a)");
		//aufgabeA(zahl);
		System.out.println("Aufgabe 4 - b)");
		aufgabeB(zahl);
		System.out.println("Aufgabe 4 - c)");
		//aufgabeC(zahl);
		System.out.println("Aufgabe 4 - d)");
		//aufgabeD(zahl);
		System.out.println("Aufgabe 4 - e)");
		//aufgabeE(zahl);
	}

	public static void aufgabeA(int i) {
		for (i = 99; i >= 9; i -= 3) {
			System.out.println(i);
		}
	}

	// FEHLT:
	public static void aufgabeB(int i) {
		// +3, +5, +7, .... i(+2)
		i = 1;
		System.out.println(i);
		for (i = 4; i < 400; i+=2) {
			System.out.println(i);
			i+=3;
		}
	}

	public static void aufgabeC(int i) {
		i = 2;
		System.out.println(i);
		while (i < 102) {
			i += 4;
			System.out.println(i);
		}
	}

	// FEHLT:
	public static void aufgabeD(int i) {
		for (i = 0; i < 1024; i++) {
			System.out.println(i);
		}
	}

	public static void aufgabeE(int i) {
		i = 2;
		System.out.println(i);
		while (i < 32768) {
			i *= 2;
			System.out.println(i);
		}
	}

}
