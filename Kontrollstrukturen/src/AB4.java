import java.util.Scanner;

public class AB4 {

	public static void main(String[] args) {
		
		int zahl1 = 0, zahl2 = 0, zahl3 = 0, antwort = 0;
		Scanner myScanner = new Scanner(System.in);
		
		antwort = input(myScanner, "Wie viele Zahlen m�chten Sie eingeben?");
		
		if(antwort == 2) {
			zahl1 = input(myScanner, " 1. Zahl: ");
			zahl2 = input(myScanner, " 2. Zahl: ");
		}
		else if (antwort == 3) {
			zahl1 = input(myScanner, " 1. Zahl: ");
			zahl2 = input(myScanner, " 2. Zahl: ");
			zahl3 = input(myScanner, " 3. Zahl: ");
		}
		
		//gleich(zahl1, zahl2);
		if(antwort == 2) {
		vergleicheZweiZahlen(zahl1, zahl2);
		}
		else if(antwort == 3) {
		vergleicheDreiZahlen(zahl1, zahl2, zahl3);
		}
	}
	
	public static void vergleicheZweiZahlen(int z1, int z2) {
		if(z1 == z2) {
			System.out.printf("Die Zahlen sind gleich ");
		}
		else {
			System.out.printf("Die Zahlen sind ungleich ");
		}
		
		if(z1 < z2) {
			System.out.println("und die 2. Zahl ist gr��er.");
		}
		else if (z1 > z2){
			System.out.println("und die 1. Zahl ist gr��er.");
		}
	}
	
	public static void vergleicheDreiZahlen(int z1, int z2, int z3) {
		if(z1 > z2 && z1 > z3) {
			System.out.printf("Die 1. Zahl ist gr��er als die anderen zwei Zahlen ");
		}
		else if (z3 > z2 || z3 > z1) {
			System.out.printf("Die 3. Zahl ist entweder gr��er als die 2. oder die 1. Zahl ");
		}
		
		if (z1 > z2 && z1 > z3) {
			System.out.println("und die 1. Zahl ist die gr��te.");
		}
		else if (z2 > z1 && z2 > z3) {
			System.out.println("und die 2. Zahl ist die gr��te.");
		}
		else if (z3 > z2 && z3 > z2) {
			System.out.println("und die 3. Zahl ist die gr��te.");
		}
	}
	
	/*public static boolean gleich(int z1, int z2) {
		if(z1 == z2) {
			return true;
		}
		else {
			return false;
		}
	}*/
	
	public static int input(Scanner scanner, String text) {
		System.out.println(text);
        int z = scanner.nextInt();
        return z;
	}

}
