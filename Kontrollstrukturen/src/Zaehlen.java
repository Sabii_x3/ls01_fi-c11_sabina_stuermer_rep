import java.util.Scanner;

// Aufgabe 1: Z�hlen (AB Schleifen 1)
public class Zaehlen {

	public static void main(String[] args) {
		int zahl = 0;

		Scanner scanner = new Scanner(System.in);

		aufgabeA(scanner, zahl);
		aufgabeB(scanner, zahl);
	}

	public static void aufgabeA(Scanner scanner, int i) {
		System.out.println("Geben Sie bitte eine Zahl f�r Aufgabe a) ein: ");
		int n = scanner.nextInt();
		for (i = 1; i <= n; i++) {
			System.out.println(i);
		}
	}

	public static void aufgabeB(Scanner scanner, int i) {
		System.out.println("Geben Sie bitte eine Zahl f�r Aufgabe b) ein: ");
		int n = scanner.nextInt();
		for (i = n; i >= 1; i--) {
			System.out.println(i);
		}
	}
}
