import java.util.Scanner;

public class HardwareGrosshaendler {

	public static void main(String[] args) {
		int menge = 0;
		double preis = 0.0; 
		final double MWST = 19.0; // Keine Variable, sondern Konstante
		
		
		Scanner myScanner = new Scanner(System.in);
		
		menge = inputMenge(myScanner, "Geben Sie bitte die Menge an M�usen ein, die Sie bestellen m�chten: ");
		preis = inputPreis(myScanner, "Geben Sie bitte den Preis f�r eine Maus ein: ");
		
		if(menge >= 10) {
			double gesamt = (menge * preis);
			double mwstBetrag = ((gesamt * MWST)/100);
			double gesamtBetrag = gesamt + mwstBetrag;
			System.out.println("Zu zahlen: " + gesamtBetrag);
		}
		else {
			double gesamt = (menge * preis);
			double mwstBetrag = ((gesamt * MWST)/100);
			double gesamtBetrag = (gesamt + mwstBetrag +10);
			System.out.println("Zu zahlen (inkl. Versandkosten): " + gesamtBetrag);
		}

	}

	public static int inputMenge(Scanner scanner, String text) {
		System.out.println(text);
        int m = scanner.nextInt();
        return m;
	}
	
	public static double inputPreis(Scanner scanner, String text) {
		System.out.println(text);
        double p = scanner.nextDouble();
        return p;
	}
	
}
