import java.util.Scanner;

public class Fakultaet {

	public static void main(String[] args) {
		int zahl = 0;

		Scanner scanner = new Scanner(System.in);

		aufgabe(scanner, zahl);

	}
	
	public static void aufgabe(Scanner scanner, int i) {
		System.out.println("Geben Sie bitte eine Zahl bis einschlie�lich 20 ein, um die Fakult�t zu ermitteln: ");
		int n = scanner.nextInt();
		i = 2;
		long ergebnis = 1;
		do {
			ergebnis *= i;
			i++;
		}
		while(i <= n);
		System.out.println(ergebnis);
	}

}
