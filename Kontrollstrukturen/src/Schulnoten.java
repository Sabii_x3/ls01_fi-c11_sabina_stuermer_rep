import java.util.Scanner;

public class Schulnoten {

	public static void main(String[] args) {
		
		
		int note = 0;
		
		Scanner myScanner = new Scanner(System.in);
		
		note = inputZahl(myScanner, "Geben Sie bitte die Schulnote als Zahl ein: ");

		switch (note){
		case 1:
			System.out.println("Sehr gut");
			break;
		case 2:
			System.out.println("Gut");
			break;
		case 3:
			System.out.println("Befriedigend");
			break;
		case 4:
			System.out.println("Ausreichend");
			break;
		case 5:
			System.out.println("Mangelhaft");
			break;
		case 6:
			System.out.println("Ungenügend");
			break;
		default:
			System.out.println("Ungültige Eingabe.");
	        }
	    }
	
	public static int inputZahl(Scanner scanner, String text) {
		System.out.println(text);
        int z = scanner.nextInt();
        return z;
	}

}
