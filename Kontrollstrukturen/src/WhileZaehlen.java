import java.util.Scanner;

// AB Schleifen 2 Aufgabe 1

public class WhileZaehlen {

	public static void main(String[] args) {
		int zahl = 0;

		Scanner scanner = new Scanner(System.in);

		aufgabeA(scanner, zahl);
		aufgabeBv(scanner, zahl);
		aufgabeBn(scanner, zahl);
	}

	// Vorpr�fend
	public static void aufgabeA(Scanner scanner, int i) {
		System.out.println("Geben Sie bitte eine Zahl f�r Aufgabe a) ein: ");
		int n = scanner.nextInt();
		i = 0;
		while (i < n) {
			i++;
			System.out.println(i);
		}
	}
	
	public static void aufgabeBv(Scanner scanner, int i) {
		System.out.println("Geben Sie bitte eine Zahl f�r Aufgabe b) vorpr�fend ein: ");
		int n = scanner.nextInt();
		i = n;
		while(i >= 1) {
			System.out.println(i);
			i--;
		}
	}
	
	// Nachpr�fend
	public static void aufgabeBn(Scanner scanner, int i) {
		System.out.println("Geben Sie bitte eine Zahl f�r Aufgabe b) nachpr�fend ein: ");
		int n = scanner.nextInt();
		i = n;
		do {
			System.out.println(i);
			i--;
		} while (i >= 1);
	}

}
